﻿USE practica1;

  /*
    consultas a realizar sobre la base de datos emple depart
  */
-- 1 Mostrar todos los campos y todos los registros de la tabla empleado
SELECT * FROM emple;

-- 2 Mostrar todos los campos y todos los registros de la tabla departamento
SELECT * FROM depart;

-- 3 Mostrar el apellido y oficio de cada empleado
SELECT apellido, oficio FROM emple;

-- 4 Mostrar localización y número de cada departamento
SELECT loc,dept_no FROM depart;

-- 5 Mostrar el número, nombre y localización de cada departamento.
SELECT dept_no, dnombre, loc FROM depart;

-- 6 Indicar el número de empleados que hay
SELECT COUNT(*) FROM emple;

-- 7 Datos de los empleados ordenados por apellido de forma ascendente
SELECT * FROM emple ORDER BY apellido ASC;

-- 8 Datos de los empleados ordenados por apellido de forma descendente
SELECT * FROM emple ORDER BY apellido DESC;

-- 9 Indicar el numero de departamentos que hay
SELECT COUNT(*) FROM depart;

-- 10 Indicar el número de empleados mas el numero de departamentos
SELECT (SELECT COUNT(*) FROM emple) + (SELECT COUNT(*) FROM depart);

-- 11 Datos de los empleados ordenados por número de departamento descendentemente
SELECT * FROM emple ORDER BY dept_no DESC;

-- 12 Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente
SELECT * FROM emple ORDER BY dept_no DESC, oficio ASC;

-- 13 Datos de los empleados ordenados por número de departamento descendentemente y por apellido
-- ascendentemente
SELECT * FROM emple ORDER BY dept_no DESC, apellido ASC;

-- 14 Mostrar los códigos de los empleados cuyo salario sea mayor que 2000
SELECT emp_no FROM emple WHERE salario>2000;

-- 15 Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000
SELECT emp_no, apellido FROM emple WHERE salario<2000;

-- 16 Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500
SELECT * FROM emple WHERE salario>1500 AND salario<2500;

-- 17 Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ
SELECT * FROM emple WHERE oficio ='ANALISTA';

-- 18 Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 €
SELECT * FROM emple WHERE oficio = 'ANALISTA' AND salario >2000;

-- 19 Seleccionar el apellido y oficio de los empleados del departamento número 20
SELECT apellido, oficio FROM emple WHERE dept_no=20;

-- 20 Contar el número de empleados cuyo oficio sea VENDEDOR
SELECT COUNT(*) FROM emple e WHERE e.oficio='VENDEDOR';


-- 21 Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de
-- forma ascendente
SELECT * FROM emple WHERE apellido LIKE 'M%' OR apellido LIKE 'N%' ORDER BY apellido ASC;


-- 22 Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ. Mostrar los datos ordenados por apellido de forma
-- ascendente.
SELECT * FROM emple e WHERE e.oficio='VENDEDOR' ORDER BY e.apellido ASC;

-- 23 Mostrar los apellidos del empleado que mas gana
SELECT MAX(SALARIO) FROM emple;
SELECT apellido FROM emple WHERE salario=(SELECT MAX(SALARIO) FROM emple);

-- 24 Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ʻANALISTAʼ. Ordenar el resultado por apellido y
-- oficio de forma ascendente
SELECT * FROM emple e WHERE e.dept_no=10 AND e.oficio='ANALISTA' ORDER BY e.apellido ASC, e.oficio ASC;

-- 25 Realizar un listado de los distintos meses en que los empleados se han dado de alta
SELECT DISTINCT MONTH(e.fecha_alt) FROM emple e;

-- 26 Realizar un listado de los distintos años en que los empleados se han dado de alta
SELECT DISTINCT year(fecha_alt) FROM emple;

-- 27 Realizar un listado de los distintos días del mes en que los empleados se han dado de alta
SELECT DISTINCT DAY(fecha_alt) FROM emple;

-- 28 Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al
-- departamento número 20.
SELECT e.apellido FROM emple e WHERE e.salario>2000;
SELECT e.apellido FROM emple e WHERE e.dept_no=20;

SELECT e.apellido FROM emple e WHERE e.salario>2000
  UNION
SELECT e.apellido FROM emple e WHERE e.dept_no=20;

-- 29 Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece
-- c1
  SELECT apellido, dept_no FROM emple;
-- c2
  SELECT apellido, d.dnombre FROM (SELECT apellido, dept_no FROM emple) c1 JOIN depart d USING(dept_no);

-- 30 Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del
-- departamento al que pertenece. Ordenar los resultados por apellido de forma descendente
-- c1
  SELECT apellido, oficio, dept_no FROM emple;
-- c2
  SELECT c1.apellido, c1.oficio, d.dnombre FROM (SELECT apellido, oficio, dept_no FROM emple) c1 JOIN depart d USING (dept_no);

-- 31 Listar el número de empleados por departamento. La salida del comando debe ser como la que vemos a
-- continuación: dept_no, numero_de_empleados
  SELECT dept_no,COUNT(*) numero_de_empleados FROM emple e GROUP BY e.dept_no;

-- 32 Realizar el mismo comando anterior pero con nombre de departamento
  -- c1
  SELECT e.dept_no, COUNT(*) numero_de_empleados FROM emple e GROUP BY e.dept_no;
  -- c2
  SELECT d.dnombre, c1.numero_de_empleados FROM (SELECT e.dept_no, COUNT(*) numero_de_empleados FROM emple e GROUP BY e.dept_no) c1 JOIN depart d USING (dept_no);

-- 33 Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre
  SELECT e.apellido FROM emple e ORDER BY e.oficio ASC, e.apellido ASC;

-- 34 Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por 'A'. Listar el apellido de los empleados
  SELECT e.apellido FROM emple e WHERE e.apellido LIKE 'A%';

-- 35 Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por 'A' o por 'M'. Listar el apellido de los empleados
  SELECT e.apellido FROM emple e WHERE e.apellido LIKE 'A%' OR e.apellido LIKE 'M%';

-- 36 Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por 'Z'. Listar todos los campos de la tabla empleados
  SELECT e.apellido FROM emple e WHERE e.apellido NOT LIKE '%Z';

-- 37 Seleccionar de la tabla EMPLE aquellas filas cuyo APELLIDO empiece por 'A' y el OFICIO tenga una 'E' en cualquier posición.
--    Ordenar la salida por oficio y por salario de forma descendente
  SELECT * FROM emple e WHERE e.apellido LIKE 'A%' AND e.oficio LIKE '%E%' ORDER BY e.oficio DESC, e.salario DESC;